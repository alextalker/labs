﻿using System;
using System.Text;
using System.Collections;
//V.6
namespace lab11
{
	public abstract class Deposit
	{
		protected int money;
		protected uint percent;
		protected DateTime start, finish;
		protected ArrayList log = new ArrayList();

//		public int Money(){
//			return money;
//		}
		public virtual uint Percent(){
			return percent;
		}

		public abstract int CurrentMoney();

		protected static double PercentToCoefficient(uint p){
			return p / 100.0;
		}
		protected double PercentToCoefficient(){
			return PercentToCoefficient(percent);
		}

		public DateTime FinishDate(){
			return finish;
		}
		protected abstract double SummaryIncome();
		public int Money {
			get {
				return this.CurrentMoney ();
			}
			set{
				if (value > 0) {
					this.money += value;
					log.Add ("Account money increment to " + value + " y.e.");
				} else if (value == 0) {
					return;
				} else if (value < 0) {
					this.money -= value;
					log.Add ("Account money decrement to " + value + " y.e.");
				} else {
					throw new Exception();
				}
			}
		}
	}

	public interface Income{
		int IncomeInRange(DateTime start, DateTime finish);
	}

	public class StaticPercentDeposit: Deposit, Income
	{
		public StaticPercentDeposit(int money, uint percent,DateTime start, DateTime finish)
		{
			this.Money = money;
			this.percent = percent;
			this.finish = finish;
			this.start = start;
		}
		public StaticPercentDeposit(int money, uint percent, DateTime finish):
		this(money,percent,DateTime.Now,finish){}
//		public StaticPercentDeposit(uint percent,DateTime start, DateTime finish):this (0, percent, start, finish){
//		}
//		public StaticPercentDeposit(uint percent, DateTime finish):this (0, percent, finish){
//		}
		public override int CurrentMoney(){
			return (int)(money + SummaryIncome());
		}
		// useless override
		protected override double SummaryIncome ()
		{
			return money * PercentToCoefficient ();
		}
		public virtual int IncomeInRange(DateTime s, DateTime f){
			if (s > f) {
				return IncomeInRange (f, s);
			} else {
				if (s < this.start) {
					s = this.start;
				}
				if (f > this.finish) {
					f = this.finish;
				}
				TimeSpan t = f - s;
				DateTime d = new DateTime (t.Ticks);
				double p = (d.Year) * PercentToCoefficient (percent);
				return (int)(money * p);
			}
		}
		public int Money{
			get{ 
				return CurrentMoney ();
			}
			set{ 
				if (value > 0) {
					money += value;
				}
			}
		}
	}
	// for payment percent after the finish of deposit
	public class EndingPercentDeposit: StaticPercentDeposit{
		public EndingPercentDeposit(int money, uint percent,DateTime start, DateTime finish):
		base(money,percent,start,finish){}
		public EndingPercentDeposit(int money, uint percent, DateTime finish):
		base(money,percent,finish){}

//		public EndingPercentDeposit(uint percent,DateTime start, DateTime finish):this(0, percent, start, finish){}
//		public EndingPercentDeposit(uint percent, DateTime finish):this(0, percent, finish){}

		public override int IncomeInRange (DateTime s, DateTime f)
		{
			if(s > f){
				return IncomeInRange (f, s);
			}else if (s == this.start && f == this.finish){
				return base.IncomeInRange (s, f);
			}
			else{
				return 0;
			}
		}

		protected override double SummaryIncome ()
		{
			if (finish <= DateTime.Now) {
				return (double)base.IncomeInRange (this.start, this.finish);
			}
			return 0;
		}

	}
	// for per year payment percents
	public class YearPercentDeposit: StaticPercentDeposit{
		public YearPercentDeposit(int money, uint percent,DateTime start, DateTime finish):
		base(money,percent,start,finish){}
		public YearPercentDeposit(int money, uint percent, DateTime finish):
		base(money,percent,finish){}

//		public YearPercentDeposit(uint percent,DateTime start, DateTime finish):this(0, percent, start, finish){}
//		public YearPercentDeposit(uint percent, DateTime finish):this(0, percent, finish){}

		public override int IncomeInRange (DateTime s, DateTime f)
		{
			TimeSpan t = f - s;
			DateTime d = new DateTime (t.Ticks);
			if ((d.Year - 1) >= 1) {
				return (base.IncomeInRange (s, f) - (int)(this.money * PercentToCoefficient(percent)));
			} else {
				return 0;
			}
		}

		protected override double SummaryIncome ()
		{
			DateTime current = (finish < DateTime.Now) ? finish : DateTime.Now;
			return (double) base.IncomeInRange(start,current);
		}
	}
	// for per month payment percents
	public class MonthPercentDeposit: StaticPercentDeposit{
		public MonthPercentDeposit(int money, uint percent,DateTime start, DateTime finish):
		base(money,percent,start,finish){}
		public MonthPercentDeposit(int money, uint percent, DateTime finish):
		base(money,percent,finish){}

		public override int IncomeInRange (DateTime s, DateTime f)
		{
			DateTime d = new DateTime ((f - s).Ticks);
			return base.IncomeInRange (s, f)/12 + money*(int)(d.Month * PercentToCoefficient(this.percent));
		}

		protected override double SummaryIncome ()
		{		
			DateTime current = (finish < DateTime.Now) ? finish : DateTime.Now;
			TimeSpan t = current - start;
			DateTime d = new DateTime (t.Ticks);
			double p = (d.Month - 1) * PercentToCoefficient (percent);
			return money * p;
		}
	}
	public struct PercentAndMoney{
		public uint Percent;
		public int Money;
		public PercentAndMoney(uint percent, int money){
			this.Percent = percent;
			this.Money = money;
		}
	}
	public static class Dynamic{
		public static uint CalculatePercent(int money, PercentAndMoney[] percents){
			bool less = false;
			for (uint i = 0; i < percents.Length; i++) {
				if (money <= percents [i].Money) {
					less = true;
				} else if (less == true) {
					return percents [i - 1].Percent;
				}
			}
			return percents[percents.Length - 1].Percent;
		}
	}
	public class DynamicMonthPercentDeposit: MonthPercentDeposit{
		public DynamicMonthPercentDeposit(int money, PercentAndMoney[] percents, DateTime start, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),start,finish){}
		public DynamicMonthPercentDeposit(int money, PercentAndMoney[] percents, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),finish){}
	}
	public class DynamicYearPercentDeposit: YearPercentDeposit{
		public DynamicYearPercentDeposit(int money, PercentAndMoney[] percents, DateTime start, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),start,finish){}
		public DynamicYearPercentDeposit(int money, PercentAndMoney[] percents, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),finish){}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			Deposit[] d = new Deposit[5];
			PercentAndMoney[] p = new PercentAndMoney[3];
			p [0] = new PercentAndMoney (10, 100);
			p [1] = new PercentAndMoney (20, 200);
			p [2] = new PercentAndMoney (30, 300);
			d [0] = new EndingPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [1] = new YearPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [2] = new MonthPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [3] = new DynamicYearPercentDeposit (110, p, new DateTime (2015, 10, 12));
			d [4] = new DynamicMonthPercentDeposit (250, p, new DateTime (2015, 10, 12));
			foreach (Deposit dp in d) {
				Console.WriteLine ("{0} money, {1} percent", dp.Money, dp.Percent());
			}
			DateTime st = new DateTime (2014, 12, 12);
			DateTime ft = new DateTime (2015, 8, 12);
			int summary = 0;
			foreach (Income i in d) {
				summary += i.IncomeInRange (st, ft);
			}
			Console.WriteLine ("Summary income: {0}",summary.ToString ());
			foreach (Deposit i in d) {
				if (i.Money % 2 == 0) {
					i.Money = 200;
				} else {
					i.Money = -100;
				}
				Console.WriteLine ("New current money: {0}", i.Money.ToString());

			}
		}
	}
}

