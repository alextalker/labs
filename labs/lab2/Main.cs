using System;

// V.28
namespace lab2
{
	class MainClass
	{
		private enum OrganizationType { ЗАО,ООО,ИП,ЧП,ОАО,ОДО, ФГУП,ГУП,МУП };
		private struct Point { 
			public double x; public double y;
			public void FillFromConsole ()
			{
				Console.WriteLine ("Please, insert x coordinate:");
				x = double.Parse (Console.ReadLine ());
				Console.WriteLine ("Please, insert y coordinate:");
				y = double.Parse (Console.ReadLine ());
			}
		}
		private struct Rectangle
		{
			public Point a,b,c;
			public void FillFromConsole ()
			{
				Console.WriteLine ("Please, insert point A coordinates:");
				a.FillFromConsole ();
				Console.WriteLine ("Please, insert point B coordinates:");
				b.FillFromConsole ();
				Console.WriteLine ("Please, insert point C coordinates:");
				c.FillFromConsole ();
			}
			private double Line (Point a, Point b)
			{
				return Math.Sqrt (Math.Pow ((b.x - a.x),2) + Math.Pow ((b.y - a.y),2));
			}
			public double Square(){
				return Line(a,b)*Line(b,c);
			}
		}
		public static void Main (string[] args)
		{
			//#1 - 8
			Console.WriteLine ("Please, insert x:");
			double x = double.Parse (Console.ReadLine ());
			Console.WriteLine ("Result: {0}", (3 * x * x + 4 * x * x - 2 * x + 1).ToString ());
			//#2 - 3
			Console.WriteLine (OrganizationType.ГУП);
			Console.WriteLine (OrganizationType.ЗАО);
			Console.WriteLine (OrganizationType.ИП);
			Console.WriteLine (OrganizationType.МУП);
			Console.WriteLine (OrganizationType.ОАО);
			Console.WriteLine (OrganizationType.ОДО);
			Console.WriteLine (OrganizationType.ООО);
			Console.WriteLine (OrganizationType.ФГУП);
			Console.WriteLine ("{0}\n",OrganizationType.ЧП);
			//#3 - 2
			Rectangle r = new Rectangle();
			r.FillFromConsole ();
			Console.WriteLine ("Square: {0}", r.Square ().ToString ());
		}
	}
}
