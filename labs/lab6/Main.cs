using System;
using System.Collections;

//V.6
namespace lab6
{
	class MainClass
	{
		protected static void PrintArray (int[] arr)
		{
			foreach (int n in arr) {
				Console.WriteLine (n);
			}
			Console.WriteLine("---");
		}
		
		protected static void PrintArray (ArrayList arr)
		{
			foreach (int n in arr) {
				Console.WriteLine (n);
			}
			Console.WriteLine("---");
		}
		public static void Main (string[] args)
		{
			// 1
			int[] a = new int[6]{34, 89, 67, 78, 45, 56};
			PrintArray (a);
			// 2
			Array.Sort (a);
			int index = Array.FindIndex (a, item => item == 67);
			Console.WriteLine ("Element index: {0} \n---", index.ToString ());
			PrintArray (a);
			//3
			ArrayList arr = new ArrayList (a);
			arr.Remove (67);
			PrintArray (arr);
			//4
			int[,] ma = new int[2, 2]{{1,1},{0,1}};
			int[,] mb = new int[2, 2]{{2,0},{3,0}};
			int[,] result = new int[2, 2] {
				 {
					(ma [0, 0] * mb [0, 0] + ma [0, 1] * mb [1, 0]),
					(ma [0, 0] * mb [0, 1] + ma [0, 1] * mb [1, 1])
				},
				 {
					(ma [1, 0] * mb [0, 0] + ma [1, 1] * mb [1, 0]),
					(ma [1, 0] * mb [0, 1] + ma [1, 1] * mb [1, 1])
				}
			};
			Console.WriteLine ("{0} {1}", result [0, 0], result [0, 1]);
			Console.WriteLine ("{0} {1}", result [1, 0], result [1, 1]);
			//5
			Hashtable t = new Hashtable (6);
			for (int i = 0; i < a.Length; i++) {
				t.Add (i, ("element " + i + ": " + a [i]));
			}
			
			foreach (DictionaryEntry d in t) {
				if (d.Key.ToString () == index.ToString()) {
					Console.WriteLine ("Result of searching is {0}", d.Value);
				}
			}
		}
	}
}
