using System;
// вариант 28

namespace lab1
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// #1
			Console.WriteLine ("Please, Insert your name:");
			string name = Console.ReadLine ();
			// #2
			Console.BackgroundColor = ConsoleColor.Blue;
			Console.ForegroundColor = ConsoleColor.White;
			// #3
			if (name.Length != 0) {
				Console.WriteLine ("Hello, {0}", name);
			}
			// #4
			Console.WriteLine ("CapsLock: {0}", Console.CapsLock ? "On" : "Off");
			Console.WriteLine ("NumLock: {0}", Console.NumberLock ? "On" : "Off");
			// #5
			Console.Beep (440, 2000);
		}
	}
}
