using System;
//V.28
namespace lab3
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//#1
			Console.WriteLine ("Please, insert magic number:");
			double number;
			while (true) {
				try {
					number = double.Parse (Console.ReadLine ());
					break;
				} catch {
					Console.WriteLine ("Wrong number!");
					continue;
				}
			}
			double i = number - 1;
			
			while (i != number) {
				try {
					Console.WriteLine ("Insert your guess:");
					i = double.Parse (Console.ReadLine ());
				} catch {
					Console.WriteLine ("Wrong number!");
					continue;
				}
				if (i > number) {
					Console.WriteLine ("Number {0} more than magic number!Try again!", i.ToString());
					continue;
				}
				if (i < number) {
					Console.WriteLine ("Number {0} less than magic number!Try again!", i.ToString());
				}
			}
			Console.WriteLine ("Number {0} are magic number!", number);
		}
	}
}
