using System;
//V.28
namespace lab4
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			double x, result;
			while (true) {
				try {
					Console.WriteLine ("Please, insert x:");
					x = double.Parse (Console.ReadLine ());
					if (x == 0) {
						throw new DivideByZeroException ();
					}
					result = ((3 * Math.Pow (x, -2)) + (4 / x) - (2 * Math.Pow (x, (1 / 2.0))) + 1);
					//Console.WriteLine ((4 / x).ToString ());
					Console.WriteLine ("Result: {0}", result);
					break;
				} catch (FormatException) {
					Console.WriteLine ("Wrong number!Use ',' to writing number with floating point!");
				} catch (DivideByZeroException) {
					Console.WriteLine ("You can't divide by zero!");
				}
			}
		}
	}
}
