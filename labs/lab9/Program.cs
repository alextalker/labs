﻿using System;
using System.Collections.Generic;

namespace lab9
{
	public abstract class Deposit
	{
		protected uint money, percent;
		protected DateTime start, finish;

		public uint Money(){
			return money;
		}
		public virtual uint Percent(){
			return percent;
		}

		public abstract uint CurrentMoney();

		protected static double PercentToCoefficient(uint p){
			return p / 100.0;
		}
		protected double PercentToCoefficient(){
			return PercentToCoefficient(percent);
		}

		public DateTime FinishDate(){
			return finish;
		}
		protected abstract double SummaryIncome();
	}

	public class StaticPercentDeposit: Deposit
	{
		public StaticPercentDeposit(uint money, uint percent,DateTime start, DateTime finish)
		{
			this.money = money;
			this.percent = percent;
			this.finish = finish;
			this.start = start;
		}
		public StaticPercentDeposit(uint money, uint percent, DateTime finish):
		this(money,percent,DateTime.Now,finish){}
		public override uint CurrentMoney(){
			return (uint)(money + SummaryIncome());
		}
		// useless override
		protected override double SummaryIncome ()
		{
			return money * PercentToCoefficient ();
		}
	}
	// for payment percent after the finish of deposit
	public class EndingPercentDeposit: StaticPercentDeposit{
		public EndingPercentDeposit(uint money, uint percent,DateTime start, DateTime finish):base(money,percent,start,finish){}
		public EndingPercentDeposit(uint money, uint percent, DateTime finish):base(money,percent,finish){}

		protected override double SummaryIncome ()
		{
			if (finish <= DateTime.Now) {
				TimeSpan t = finish - start;
				DateTime d = new DateTime (t.Ticks);
				double p = (d.Year - 1) * PercentToCoefficient (percent);
				return money * p;
			}
			return 0;
		}
	}
	// for per year payment percents
	public class YearPercentDeposit: StaticPercentDeposit{
		public YearPercentDeposit(uint money, uint percent,DateTime start, DateTime finish):
		base(money,percent,start,finish){}
		public YearPercentDeposit(uint money, uint percent, DateTime finish):
		base(money,percent,finish){}

		protected override double SummaryIncome ()
		{
			DateTime current = (finish < DateTime.Now) ? finish : DateTime.Now;
			TimeSpan t = current - start;
			DateTime d = new DateTime (t.Ticks);
			double p = (d.Year - 1) * PercentToCoefficient (percent);
			return money * p;
		}
	}
	// for per month payment percents
	public class MonthPercentDeposit: StaticPercentDeposit{
		public MonthPercentDeposit(uint money, uint percent,DateTime start, DateTime finish):
		base(money,percent,start,finish){}
		public MonthPercentDeposit(uint money, uint percent, DateTime finish):
		base(money,percent,finish){}

		protected override double SummaryIncome ()
		{		
			DateTime current = (finish < DateTime.Now) ? finish : DateTime.Now;
			TimeSpan t = current - start;
			DateTime d = new DateTime (t.Ticks);
			double p = (d.Month - 1) * PercentToCoefficient (percent);
			return money * p;
		}
	}
	public struct PercentAndMoney{
		public uint Percent;
		public uint Money;
		public PercentAndMoney(uint percent, uint money){
			this.Percent = percent;
			this.Money = money;
		}
	}
	public static class Dynamic{
		public static uint CalculatePercent(uint money, PercentAndMoney[] percents){
			bool less = false;
			for (uint i = 0; i < percents.Length; i++) {
				if (money <= percents [i].Money) {
					less = true;
				} else if (less == true) {
					return percents [i - 1].Percent;
				}
			}
			return percents[percents.Length - 1].Percent;
		}
	}
	public class DynamicMonthPercentDeposit: MonthPercentDeposit{
		public DynamicMonthPercentDeposit(uint money, PercentAndMoney[] percents, DateTime start, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),start,finish){}
		public DynamicMonthPercentDeposit(uint money, PercentAndMoney[] percents, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),finish){}
	}
	public class DynamicYearPercentDeposit: YearPercentDeposit{
		public DynamicYearPercentDeposit(uint money, PercentAndMoney[] percents, DateTime start, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),start,finish){}
		public DynamicYearPercentDeposit(uint money, PercentAndMoney[] percents, DateTime finish):
		base(money,Dynamic.CalculatePercent(money,percents),finish){}
	}
	class MainClass
	{
		public static void Main (string[] args)
		{
			Deposit[] d = new Deposit[5];
			PercentAndMoney[] p = new PercentAndMoney[3];
			p [0] = new PercentAndMoney (10, 100);
			p [1] = new PercentAndMoney (20, 200);
			p [2] = new PercentAndMoney (30, 300);
			d [0] = new EndingPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [1] = new YearPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [2] = new MonthPercentDeposit (100, 10, new DateTime (2015, 10, 12));
			d [3] = new DynamicYearPercentDeposit (110, p, new DateTime (2015, 10, 12));
			d [4] = new DynamicMonthPercentDeposit (250, p, new DateTime (2015, 10, 12));
			foreach (Deposit dp in d) {
				Console.WriteLine ("{0} money, {1} percent", dp.Money(), dp.Percent());
			}
		}
	}
}
