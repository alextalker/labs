﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace lab13
{
	class MainClass
	{
		public static List<List<int>> FileParser (string PathToFile)
		{
			List<List<int>> result = new List<List<int>>();
			if (File.Exists (PathToFile)) {
				StreamReader file = null;
				try{
					file = new StreamReader(PathToFile);
					string line;
					while((line = file.ReadLine()) != null){
						//stub
						if(line.Length == 1){
							if( line[0] != ' ' ){
								List<int> parsed = new List<int>();
								parsed.Add(int.Parse(line));
								result.Add(parsed);
							}else{
								continue;
							}
						}else{
							List<int> parsed = new List<int>();
							bool IsNotSpace = false;
							int start_substing = 0;
							for(int i = 0; i < line.Length;i++){
								if( line[i] == ' ' ){
									if(IsNotSpace){
										parsed.Add(int.Parse(
											line.Substring(start_substing,(i - start_substing))
										));// обработай тут исключение, плз
									}
									IsNotSpace = false;
									continue;
								}
								if(!IsNotSpace){
									IsNotSpace = true;
									start_substing = i;
									continue;
								}
							}
							if(IsNotSpace){
								parsed.Add(int.Parse(
									line.Substring(start_substing,(line.Length - start_substing))
								));// и тут
							}
							result.Add(parsed);
						}
					}
				}
				finally{
					if (file != null) {
						file.Close ();
					}
				}
			} else {
				throw new Exception ();
			}
			return result;
		}
		public static void Main (string[] args)
		{
			// ИДИОТСКИЙ ТЕСТ, просто подставь путь к файлу.Это консольное приложение, ага.
//			foreach (ArrayList a in FileParser("foo.txt")) {
//				foreach (int i in a) {
//					Console.Write (i.ToString() + ' ');
//				}
//				Console.Write ('\n');
//			}
			string[] numbers = {"One","Two","Three","Four","Five"};
			List<List<int>> array = FileParser ("foo.txt");
			Console.WriteLine (array[0][0]);
		}
	}
}
