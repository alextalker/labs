using System;
//V.28
namespace lab5
{
	class MainClass
	{
		private static Point FillFromConsole (Point p)
		{
			while (true) {
				try {
					Console.WriteLine ("Please, insert coordinate x:");
					p.x = double.Parse (Console.ReadLine ());
					Console.WriteLine ("Please, insert coordinate y:");
					p.y = double.Parse (Console.ReadLine ());
					break;
				} catch {
					Console.WriteLine ("Wrong input!Try again!");
					continue;
				}
			}
			return p;
		}
		private static Square FillFromConsole (Square s)
		{
			while (true) {
				try {
					Console.WriteLine ("Please, insert coordinates of point A for square:");
					s.a = FillFromConsole (s.a);
					Console.WriteLine ("Please, insert coordinates of point B for square:");
					s.b = FillFromConsole (s.b);
					break;
				} catch {
					Console.WriteLine ("Wrong input!Try again!");
					continue;
				}
			}
			return s;
		}
		public static void Main (string[] args)
		{
			// #1
			Square square = new Square();
			square = FillFromConsole (square);
			Console.WriteLine ("Square: {0}", square.Sq ().ToString ());
			Console.WriteLine ("Perimeter: {0}", square.Perimetr ().ToString ());
			//#2 - 1
			double x;
			while (true) {
				try {
					Console.WriteLine ("Please, insert x to calculate function:");
					x = double.Parse (Console.ReadLine ());
					break;
				} catch {
					Console.WriteLine ("Wrong number, please, try again!");
					continue;
				}
			}
			Console.WriteLine ("Result: {0}", Other.Func (x));
			//#2 - 2
			Console.WriteLine ("Sum: {0}", Other.Sum (1, 2, 3, 4, 5));
			//#2 - 4
			uint n = 0;
			while (true) {
				try {
					Console.WriteLine ("Please, insert number to calculate factorial:");
					n = uint.Parse (Console.ReadLine ());
					break;
				} catch {
					Console.WriteLine ("Wrong number, please, try again!");
					continue;
				}
			}
			Console.WriteLine ("Factorial: {0}", Other.Fact (n));
		}
	}
}
