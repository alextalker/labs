using System;
namespace lab5
{
	//#1 - 3
	public struct Point
	{
		public double x,y;
	}
	public struct Square
	{
		public Point a,b;
		private double Length ()
		{
			return Math.Sqrt (Math.Pow ((a.x - b.x), 2) + Math.Pow ((a.y - b.y), 2));
		}
		public double Sq(){
			return Length()*Length();
		}
		public double Perimetr ()
		{
			return 4 * Length ();
		}
	}
	public struct Other{
		//#1
		public static double Func (double x)
		{
			return Math.Pow (x, 2) - Math.Pow ((x - 2), 2);
		}
		//#2
		public static double Sum (params double[] numbers)
		{
			double result = 0;
			for (int i = 0; i < numbers.Length; i++) {
				result += numbers [i];
			}
			return result;
		}
		//#4
		public static double Fact (uint n)
		{
			uint result = 1;
			try {
				for (; n > 0; n--) {
					result *= n;
				}
			} catch {
				Console.WriteLine ("Wrong factorial, result incorrect.");
				result = 1;
			}
			return result;
		}
	}
}

