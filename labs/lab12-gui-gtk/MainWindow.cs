﻿using System;
using lab12guigtk;
using System.Text;
using Gtk;

public partial class MainWindow: Gtk.Window
{
	public MainWindow () : base (Gtk.WindowType.Toplevel)
	{
//		text.Buffer.Text = "Please, insert key length here if you want to generate new";
//		keytext.Buffer.Text = "Please, insert exit here if you want encrypt/decrypt text from first text form.";
//		textoutput.Buffer.Text = "Encryptec/decrypted text will be displayed here!";
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
	protected virtual void OnNewKeyCliecked(object sender, System.EventArgs e){
		int keylength;
		if(text.Buffer.Text.Length > 0){
			try{
				keylength = int.Parse(text.Buffer.Text);
			}
			catch(System.FormatException){
				text.Buffer.Text = "This value is wrong:" + text.Buffer.Text;
				return;
			}
			catch(OverflowException){
				text.Buffer.Text = "This value is too large:" + text.Buffer.Text;
				return;
			}
		}else{
			keylength = 1024;
		}
		if (keylength <= 0) {
			keylength = 1024;
		}
		lab12guigtk.Key k = new lab12guigtk.Key (keylength);
		text.Buffer.Text = k.PKToBase64 ();
		keytext.Buffer.Text = k.FKToBase64 ();
		textoutput.Buffer.Text = "Use this public and full key to encrypt messages!";
	}
	protected virtual void OnEncryptCliecked(object sender, System.EventArgs e){
		Console.WriteLine ("Into encrypt process...");// DEBUG
		string text_to_encrypt = text.Buffer.Text;
		lab12guigtk.Key k = new lab12guigtk.Key (Encrypt.GetString(Convert.FromBase64String(keytext.Buffer.Text)));
		textoutput.Buffer.Text = Encrypt.EncryptString (text_to_encrypt, k.KeyLength, k.PublicKey);
	}
	protected virtual void OnDecryptCliecked(object sender, System.EventArgs e){
		Console.WriteLine ("Into decrypt process...");// DEBUG
		string text_to_decrypt = text.Buffer.Text;
		lab12guigtk.Key k = new lab12guigtk.Key (Encrypt.GetString(Convert.FromBase64String(keytext.Buffer.Text)));
		textoutput.Buffer.Text = Encrypt.DecryptString (text_to_decrypt, k.KeyLength, k.FullKey);
	}
}
