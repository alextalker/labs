using System;
using System.Text;

//V.6
namespace lab8
{
	class MainClass
	{
		protected static string ReverseString (string str)
		{
			StringBuilder s = new StringBuilder (str);
			char c;
			int length = s.Length - 1;
			for (int i = 0; i < (s.Length/2); i++) {
				c = s [i];
				s [i] = s [length - i];
				s [length - i] = c;
			}
			return s.ToString();
		}
		protected static string ReverseCase (string str)
		{
			StringBuilder s = new StringBuilder (str);
			for (int i = 0; i < s.Length; i++) {
				char c = s [i];
				if (char.IsUpper (c)) {
					c = char.ToLower (c);
				} else if (char.IsLower (c)) {
					c = char.ToUpper (c);
				}
				s [i] = c;
			}
			return s.ToString ();
		}
		public static void Main (string[] args)
		{
			Console.WriteLine ("Please, insert string:");
			string s = Console.ReadLine ();
			//1
			Console.WriteLine ("Please, insert substring for search:");
			string sub = Console.ReadLine ();
			Console.WriteLine ("Поиск с начала строки, начальный индекс: {0}", s.IndexOf (sub).ToString ());
			Console.WriteLine ("Поиск с конца строки, начальный индекс: {0}", s.LastIndexOf (sub).ToString ());
			//2
			char c = ' ';
			string[] splitted = s.Split (c);
			Console.WriteLine ("Splitted strings:");
			foreach (string str in splitted) {
				Console.WriteLine (str);
			}
			//4
			int number = 42;
			double num = 42.42;
			DateTime date = new DateTime (2014, 9, 30, 20, 36, 47);
			Console.WriteLine ("{0:D}, {1:e2}, {2:000}", number, number, number);
			Console.WriteLine ("{0:0.00}, {1:e2}, {2:0.0}", num, num, num);
			Console.WriteLine("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n", date.ToString("d"), date.ToString("D"), date.ToString("F"), date.ToString("g"), date.ToString("u"), date.ToString("T"));
			//6
			Console.WriteLine (ReverseCase (s));
			//7
			Console.WriteLine (ReverseString (s));
		}
	}
}
